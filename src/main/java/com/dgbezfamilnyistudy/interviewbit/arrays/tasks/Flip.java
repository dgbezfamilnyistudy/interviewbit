package com.dgbezfamilnyistudy.interviewbit.arrays.tasks;

import java.util.ArrayList;
import java.util.List;

/*
You are given a binary string(i.e. with characters 0 and 1) S consisting of characters S1, S2, …, SN. In a single operation, you can choose two indices L and R such that 1 ≤ L ≤ R ≤ N and flip the characters SL, SL+1, …, SR. By flipping, we mean change character 0 to 1 and vice-versa.

Your aim is to perform ATMOST one operation such that in final string number of 1s is maximised. If you don’t want to perform the operation, return an empty array. Else, return an array consisting of two elements denoting L and R. If there are multiple solutions, return the lexicographically smallest pair of L and R.

Notes:

Pair (a, b) is lexicographically smaller than pair (c, d) if a < c or, if a == c and b < d.
For example,

S = 010

Pair of [L, R] | Final string
_______________|_____________
[1 1]          | 110
[1 2]          | 100
[1 3]          | 101
[2 2]          | 000
[2 3]          | 001

We see that two pairs [1, 1] and [1, 3] give same number of 1s in final string. So, we return [1, 1].
Another example,

If S = 111

No operation can give us more than three 1s in final string. So, we return empty array [].
* */

public class Flip {
    public static void main(String[] args) {
        Flip flip = new Flip();
        System.out.println(flip.flip("101100"));
    }

    public List<Integer> flip(String sourceString) {
        List<Integer> sourceList = getIntegers(sourceString);
        int currentSum = 0, maxSum = 0;
        int currentStartIndex = 1, currentEndIndex;
        int maxStartIndex = -1, maxEndIndex = -1;
        int currentNumber;
        for (int i = 0; i < sourceList.size(); i++) {
            currentEndIndex = i + 1;
            currentNumber = convertForLocalCalculate(sourceList.get(i));
            currentSum += currentNumber;
            currentStartIndex = updateCurrentStartIndex(currentSum, currentEndIndex, currentStartIndex);
            currentSum = updateCurrentSum(currentSum);
            if (maxSum < currentSum) {
                maxStartIndex = currentStartIndex;
                maxEndIndex = currentEndIndex;
                maxSum = currentSum;
            }
        }

        return createRes(maxStartIndex, maxEndIndex);
    }

    private List<Integer> createRes(int maxStartIndex, int maxEndIndex) {
        ArrayList<Integer> res = new ArrayList<>();
        if (maxStartIndex > 0 && maxEndIndex > 0) {
            res.add(maxStartIndex);
            res.add(maxEndIndex);
        }
        return res;
    }

    private int updateCurrentSum(int currentSum) {
        return currentSum < 0 ? 0 : currentSum;
    }

    private int updateCurrentStartIndex(int currentSum, int currentEndIndex, int currentStartIndex) {
        return currentSum < 0 ? currentEndIndex + 1 : currentStartIndex;
    }

    private int convertForLocalCalculate(int numberFromList) {
        return numberFromList == 0 ? 1 : -1;
    }

    private List<Integer> getIntegers(String sourceString) {
        char[] sourceCharArray = sourceString.toCharArray();
        List<Integer> sourceIntList = new ArrayList<>();
        for (char character : sourceCharArray) {
            sourceIntList.add(Character.getNumericValue(character));
        }
        return sourceIntList;
    }
}
