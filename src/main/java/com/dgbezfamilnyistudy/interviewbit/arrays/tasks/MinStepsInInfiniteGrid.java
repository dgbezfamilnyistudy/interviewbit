package com.dgbezfamilnyistudy.interviewbit.arrays.tasks;

import java.util.Arrays;
import java.util.List;

/*
You are in an infinite 2D grid where you can move in any of the 8 directions :

 (x,y) to
    (x+1, y),
    (x - 1, y),
    (x, y+1),
    (x, y-1),
    (x-1, y-1),
    (x+1,y+1),
    (x-1,y+1),
    (x+1,y-1)
You are given a sequence of points and the order in which you need to cover the points. Give the minimum number of steps in which you can achieve it. You start from the first point.

Input :

Given two integer arrays A and B, where A[i] is x coordinate and B[i] is y coordinate of ith point respectively.
Output :

Return an Integer, i.e minimum number of steps.
Example :

Input : [(0, 0), (1, 1), (1, 2)]
Output : 2
It takes 1 step to move from (0, 0) to (1, 1). It takes one more step to move from (1, 1) to (1, 2).
*
* */

public class MinStepsInInfiniteGrid {
    public static void main(String[] args) {
        List<Integer> A = Arrays.asList(4, 8, -7, -5, -13, 9, -7, 8);
        List<Integer> B = Arrays.asList(4, -15, -10, -3, -13, 12, 8, -8);
        int res = coverPoints(A, B);
        System.out.println(res);
    }

    public static int coverPoints(List<Integer> A, List<Integer> B) {
        int currentX = A.get(0);
        int currentY = B.get(0);
        int lenX;
        int lenY;
        int allLength = 0;
        for (int i = 1; i < A.size(); i++) {
            lenX = Math.abs(currentX - A.get(i));
            lenY = Math.abs(currentY - B.get(i));
            currentX = A.get(i);
            currentY = B.get(i);
            allLength += max(lenX, lenY);
        }
        return allLength;
    }

    private static int max(int lenX, int lenY) {
        return lenX > lenY ? lenX : lenY;
    }
}
