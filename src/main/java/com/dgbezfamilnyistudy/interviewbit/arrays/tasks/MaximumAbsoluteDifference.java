package com.dgbezfamilnyistudy.interviewbit.arrays.tasks;

import java.util.Arrays;
import java.util.List;

/*
*
	Maximum Absolute Difference

	You are given an array of N integers, A1, A2 ,…, AN. Return maximum value of f(i, j) for all 1 ≤ i, j ≤ N.
	f(i, j) is defined as |A[i] - A[j]| + |i - j|, where |x| denotes absolute value of x.

	For example,

	A=[1, 3, -1]

	f(1, 1) = f(2, 2) = f(3, 3) = 0
	f(1, 2) = f(2, 1) = |1 - 3| + |1 - 2| = 3
	f(1, 3) = f(3, 1) = |1 - (-1)| + |1 - 3| = 4
	f(2, 3) = f(3, 2) = |3 - (-1)| + |2 - 3| = 5

	So, we return 5.
*
*
* */

public class MaximumAbsoluteDifference {
	public static void main(String[] args) {
		List<Integer> integers = Arrays.asList( 1, 3, -1 );
		System.out.println( maxArr( integers ) );
	}

	public static int maxArr(List<Integer> array) {
		int max1 = Integer.MIN_VALUE;
		int min1 = Integer.MAX_VALUE;
		int max2 = Integer.MIN_VALUE;
		int min2 = Integer.MAX_VALUE;

		int currentCalculatedElement;
		for ( int i = 0; i < array.size(); i++ ) {
			currentCalculatedElement = array.get( i ) + i;
			max1 = Math.max( max1, currentCalculatedElement );
			min1 = Math.min( min1, currentCalculatedElement );

			currentCalculatedElement = array.get( i ) - i;
			max2 = Math.max( max2, currentCalculatedElement );
			min2 = Math.min( min2, currentCalculatedElement );
		}

		int maxAbsoluteDiffCase1 = max1 - min1;
		int maxAbsoluteDiffCase2 = max2 - min2;

		return Math.max( maxAbsoluteDiffCase1, maxAbsoluteDiffCase2 );
	}
}
