package com.dgbezfamilnyistudy.interviewbit.arrays.tasks;

/*
You are given a read only array of n integers from 1 to n.

Each integer appears exactly once except A which appears twice and B which is missing.

Return A and B.

Note: Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?

Note that in your output A should precede B.

Example:

Input:[3 1 2 5 3]

Output:[3, 4]

A = 3, B = 4
* */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RepeatAndMissingNumberArray {

    public static void main(String[] args) {
        RepeatAndMissingNumberArray repeatAndMissingNumberArray = new RepeatAndMissingNumberArray();
        System.out.println(repeatAndMissingNumberArray.repeatedNumber(Arrays.asList(5, 2, 5, 3, 4, 7, 1)));
    }


    // DO NOT MODIFY THE LIST. IT IS READ ONLY
    public ArrayList<Integer> repeatedNumber(final List<Integer> A) {
        long actualSum = 0;
        long actualSumInSquare = 0;
        for (Integer elInt : A) {
            actualSum += elInt;
            actualSumInSquare += elInt * elInt;
        }
        long expectedSum = getExpectedSum(A);
        long expectedSumInSquare = getExpectedSumInSquare(A);

        ArrayList<Integer> res = new ArrayList<>();
        Integer missedElement = findMissedElement(actualSumInSquare, expectedSumInSquare, actualSum, expectedSum);
        Integer duplicatedElement = findDuplicatedElement(actualSum, expectedSum, missedElement);
        res.add(duplicatedElement);
        res.add(missedElement);

        return res;
    }

    private Integer findDuplicatedElement(long actualSum, long expectedSum, Integer missedElement) {
        return (int) (actualSum - expectedSum + missedElement.longValue());
    }

    private Integer findMissedElement(long actualSumInSquare, long expectedSumSquare, long actualSum, long expectedSum) {
        return (int) ((actualSumInSquare - expectedSumSquare - actualSum * actualSum - expectedSum * expectedSum + 2 * actualSum * expectedSum) / (2 * (actualSum - expectedSum)));
    }

    private long getExpectedSum(final List<Integer> list) {
        double n = list.size();
        return (long) (n * (n - 1) / 2 + n);
    }

    private long getExpectedSumInSquare(final List<Integer> list) {
        double n = list.size();
        return (long) (n * (n + 1) * (2 * n + 1) / 6);
    }
}
