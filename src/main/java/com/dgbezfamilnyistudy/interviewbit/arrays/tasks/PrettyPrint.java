package com.dgbezfamilnyistudy.interviewbit.arrays.tasks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/*
Print concentric rectangular pattern in a 2d matrix.
Let us show you some examples to clarify what we mean.

Example 1:

Input: A = 4.
Output:

4 4 4 4 4 4 4
4 3 3 3 3 3 4
4 3 2 2 2 3 4
4 3 2 1 2 3 4
4 3 2 2 2 3 4
4 3 3 3 3 3 4
4 4 4 4 4 4 4
Example 2:

Input: A = 3.
Output:

3 3 3 3 3
3 2 2 2 3
3 2 1 2 3
3 2 2 2 3
3 3 3 3 3
The outermost rectangle is formed by A, then the next outermost is formed by A-1 and so on.

You will be given A as an argument to the function you need to implement, and you need to return a 2D array.
* */
public class PrettyPrint {
    public static void main(String[] args) {
        PrettyPrint prettyPrint = new PrettyPrint();
        System.out.println(prettyPrint.prettyPrint(4));
    }

    public ArrayList<ArrayList<Integer>> prettyPrint(int A) {
        int countStringAndColumns = (A - 1) * 2 + 1;
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        ArrayList<Integer> line;
        int numberForAdd;
        int minNumberForAdd;
        for (int i = 0; i < countStringAndColumns; i++) {
            if (i <= countStringAndColumns / 2) {
                line = new ArrayList<>();
                res.add(line);
                numberForAdd = A;
                minNumberForAdd = A - i;
                for (int j = 0; j <= countStringAndColumns / 2; j++) {
                    line.add(numberForAdd);
                    if (numberForAdd > minNumberForAdd) {
                        numberForAdd--;
                    }
                }
                line.addAll(createSecondPartOfLine(line));
            } else {
                res.add(getLineForDuplication(res, countStringAndColumns - i - 1));
            }
        }

        return res;
    }

    private ArrayList<Integer> getLineForDuplication(ArrayList<ArrayList<Integer>> res, int indexExistedLine) {
        return res.get(indexExistedLine);
    }

    private Collection<? extends Integer> createSecondPartOfLine(ArrayList<Integer> line) {
        return reverse(new ArrayList<>(line.subList(0, line.size() - 1)));
    }

    private Collection<? extends Integer> reverse(List<Integer> line) {
        Collections.reverse(line);
        return line;
    }
}
