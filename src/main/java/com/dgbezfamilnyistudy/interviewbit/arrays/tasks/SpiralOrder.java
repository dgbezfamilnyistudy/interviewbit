package com.dgbezfamilnyistudy.interviewbit.arrays.tasks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SpiralOrder {
    private static final int LEFT_TO_RIGHT = 0;
    private static final int TOP_TO_BOTTOM = 1;
    private static final int RIGHT_TO_LEFT = 2;
    private static final int BOTTOM_TO_TOP = 3;

    public static void main(String[] args) {
        List<List<Integer>> array2D = new ArrayList<>();
        List<Integer> row = Arrays.asList(1, 2, 3);
        array2D.add(row);
        row = Arrays.asList(4, 5, 6);
        array2D.add(row);
        row = Arrays.asList(7, 8, 9);
        array2D.add(row);
        System.out.println(spiralOrder(array2D));
    }

    public static List<Integer> spiralOrder(final List<List<Integer>> A) {
        List<Integer> res = new ArrayList<>();
        int topRow = 0;
        int bottomRow = A.size() - 1;
        int leftColumn = 0;
        int rightColumn = A.get(0).size() - 1;
        int dir = 0;
        while (topRow <= bottomRow && leftColumn <= rightColumn) {
            switch (dir) {
                case LEFT_TO_RIGHT:
                    for (int i = leftColumn; i <= rightColumn; i++) {
                        res.add(A.get(topRow).get(i));
                    }
                    topRow++;
                    break;
                case TOP_TO_BOTTOM:
                    for (int i = topRow; i <= bottomRow; i++) {
                        res.add(A.get(i).get(rightColumn));
                    }
                    rightColumn--;
                    break;
                case RIGHT_TO_LEFT:
                    for (int i = rightColumn; i >= leftColumn; i--) {
                        res.add(A.get(bottomRow).get(i));
                    }
                    bottomRow--;
                    break;
                case BOTTOM_TO_TOP:
                    for (int i = bottomRow; i >= topRow; i--) {
                        res.add(A.get(i).get(leftColumn));
                    }
                    leftColumn++;
                    break;
            }
            dir = (dir + 1) % 4;
        }
        return res;
    }
}
