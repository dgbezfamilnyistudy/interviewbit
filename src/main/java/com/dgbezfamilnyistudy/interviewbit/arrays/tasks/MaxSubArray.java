package com.dgbezfamilnyistudy.interviewbit.arrays.tasks;

import java.util.Arrays;
import java.util.List;

import static java.lang.Math.max;

/*
Max Sum Contiguous Subarray

		 Find the contiguous subarray within an array (containing at least one number) which has the largest sum.

		 For example:

		 Given the array [-2,1,-3,4,-1,2,1,-5,4],

		 the contiguous subarray [4,-1,2,1] has the largest sum = 6.

		 For this problem, return the maximum sum.
*/


public class MaxSubArray {
	public static void main(String[] args) {
		List<Integer> a = Arrays.asList( 9, -1, 1, 2, 1 );
		MaxSubArray maxSubArray = new MaxSubArray();
		System.out.println( maxSubArray.maxSubArray( a ) );
	}

	public int maxSubArray(final List<Integer> A) {
		return maxSubArray( A, 0, A.size() );
	}

	private int maxSubArray(List<Integer> arr, int start, int n) {
		if ( n == 1 ) {
			return arr.get( start );
		}
		int middle = n / 2;
		int maxSubArrayLeft = maxSubArray( arr, start, middle );
		int maxSubArrayRight = maxSubArray( arr, start + middle, n - middle );
		int maxLeftSum = Integer.MIN_VALUE;
		int sum = 0;
		for ( int i = start + middle - 1; i >= start; i-- ) {
			sum += arr.get( i );
			if ( sum > maxLeftSum ) {
				maxLeftSum = sum;
			}
		}
		int maxRightSum = Integer.MIN_VALUE;
		sum = 0;
		for ( int i = start + middle; i < start + n; i++ ) {
			sum += arr.get( i );
			if ( sum > maxRightSum ) {
				maxRightSum = sum;
			}
		}
		return max( max( maxSubArrayRight, maxSubArrayLeft ), ( maxLeftSum + maxRightSum ) );
	}
}
