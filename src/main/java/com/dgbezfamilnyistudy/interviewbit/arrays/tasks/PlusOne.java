package com.dgbezfamilnyistudy.interviewbit.arrays.tasks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlusOne {
    public static void main(String[] args) {
        PlusOne plusOne = new PlusOne();
        ArrayList<Integer> integers = new ArrayList<>(Arrays.asList(0, 0, 4, 4, 6, 0, 9, 6, 5, 1));
        plusOne.plusOne(integers);
        System.out.println(integers);
    }

    public static ArrayList<Integer> plusOne(ArrayList<Integer> digits) {
        Integer digit = digits.get(digits.size() - 1);
        int index = digits.size() - 1;
        while (isLimitNumber(digit) && index >= 0) {
            digits.set(index, 0);
            index--;
            if (index >= 0) {
                digit = digits.get(index);
            }
        }
        if (index < 0) {
            digits.add(0, 1);
        } else {
            digits.set(index, digit + 1);
        }
        deleteUnnecessaryZeros(digits);
        return digits;
    }

    private static boolean isLimitNumber(Integer digit) {
        return digit.equals(9);
    }

    private static void deleteUnnecessaryZeros(List<Integer> digits) {
        while (digits.get(0).equals(0)) {
            digits.remove(0);
        }
    }
}
