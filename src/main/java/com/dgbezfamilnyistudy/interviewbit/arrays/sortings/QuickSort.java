package com.dgbezfamilnyistudy.interviewbit.arrays.sortings;

import java.util.Arrays;

public class QuickSort {
    public static void main(String[] args) {
        int[] array = {-3, 12, 0, 1, 34, 5};
        quickSort(array, 0, array.length - 1);
        System.out.println(Arrays.toString(array));
    }

    private static void quickSort(int[] array, int start, int end) {
        if (start >= end) {
            return;
        }
        int pIndex = partition(array, start, end);
        quickSort(array, start, pIndex - 1);
        quickSort(array, pIndex + 1, end);
    }

    private static int partition(int[] array, int start, int end) {
        int pivot = array[end];
        int futurePIndex = start;
        for (int i = start; i < end; i++) {
            if (array[i] <= pivot) {
                swap(array, futurePIndex, i);
                futurePIndex++;
            }
        }
        swap(array, futurePIndex, end);
        return futurePIndex;
    }

    private static void swap(int[] array, int firstIndex, int secondIndex) {
        int temp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = temp;
    }
}
