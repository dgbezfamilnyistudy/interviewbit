package com.dgbezfamilnyistudy.interviewbit.arrays.sortings;

import java.util.Arrays;

public class MergeSort {
	public static void main(String[] args) {
		int[] array = { 1, 4, 5, 3, 1, 0, 2 };
		mergeSort( array );
		System.out.println( Arrays.toString( array ) );
	}

	private static void mergeSort(int[] array) {
		int middleIndex = array.length / 2;
		if ( middleIndex < 1 ) {
			return;
		}
		int[] left = Arrays.copyOfRange( array, 0, middleIndex );
		int[] right = Arrays.copyOfRange( array, middleIndex, array.length );
		mergeSort( left );
		mergeSort( right );
		merge( left, right, array );
	}

	private static void merge(int[] firstPart, int[] secondPart, int[] destination) {
		int firstIndex = 0;
		int secondIndex = 0;
		int destinationIndex = 0;
		while ( firstIndex < firstPart.length && secondIndex < secondPart.length ) {
			if ( firstPart[firstIndex] < secondPart[secondIndex] ) {
				destination[destinationIndex] = firstPart[firstIndex];
				firstIndex++;
			}
			else {
				destination[destinationIndex] = secondPart[secondIndex];
				secondIndex++;
			}
			destinationIndex++;
		}
		while ( firstIndex < firstPart.length ) {
			destination[destinationIndex++] = firstPart[firstIndex++];
		}
		while ( secondIndex < secondPart.length ) {
			destination[destinationIndex++] = secondPart[secondIndex++];
		}
	}
}
