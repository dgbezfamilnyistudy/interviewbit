package com.dgbezfamilnyistudy.interviewbit.arrays.sortings;

import java.util.Arrays;

public class InsertionSort {
	public static void main(String[] args) {
		int[] array = { 4, 5, 2, 4, 10 };
		sort( array );
		System.out.println( Arrays.toString( array ) );
	}

	private static void sort(int[] array) {
		for ( int i = 1; i < array.length; i++ ) {
			int j = i;
			int currentValue = array[j];
			while ( j > 0 && currentValue < array[j - 1] ) {
				array[j] = array[j - 1];
				j--;
			}
			array[j] = currentValue;
		}
	}
}

