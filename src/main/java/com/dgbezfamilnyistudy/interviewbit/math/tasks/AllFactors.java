package com.dgbezfamilnyistudy.interviewbit.math.tasks;

import java.util.ArrayList;
import java.util.List;

/*
All Factors

		Given a number N, find all factors of N.

		Example:

		N = 6
		factors = {1, 2, 3, 6}
		Make sure the returned array is sorted.
*/
public class AllFactors {
	public static void main(String[] args) {
		System.out.println( allFactors( 25 ) );
	}

	public static List<Integer> allFactors(int number) {
		List<Integer> res = new ArrayList<>();
		for ( int i = 1, insertIndex = 0; i <= Math.sqrt( number ); i++ ) {
			if ( number % i == 0 ) {
				res.add( insertIndex, i );
				if ( i != Math.sqrt( number ) ) {
					res.add( res.size() - insertIndex, number / i );
				}
				insertIndex++;
			}
		}

		return res;
	}
}
