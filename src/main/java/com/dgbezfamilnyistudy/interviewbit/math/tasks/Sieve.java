package com.dgbezfamilnyistudy.interviewbit.math.tasks;

import java.util.ArrayList;
import java.util.List;

/*
Prime Numbers

Given a number N, find all prime numbers upto N ( N included ).

		Example:

		if N = 7,

		all primes till 7 = {2, 3, 5, 7}

		Make sure the returned array is sorted.
*/
public class Sieve {
	public static void main(String[] args) {
		System.out.println( sieve( 15 ) );
	}

	public static List<Integer> sieve(int number) {
		int[] indicates = new int[number + 1];
		for ( int i = 0; i < indicates.length; i++ ) {
			indicates[i] = 1;
		}
		indicates[0] = 0;
		indicates[1] = 0;

		for ( int i = 2; i <= Math.sqrt( number ); i++ ) {
			if ( indicates[i] == 1 ) {
				for ( int j = 2; i * j <= number; j++ ) {
					indicates[i * j] = 0;
				}
			}
		}

		List<Integer> res = new ArrayList<>();
		for ( int i = 2; i < indicates.length; i++ ) {
			if ( indicates[i] == 1 ) {
				res.add( i );
			}
		}
		return res;
	}


}
