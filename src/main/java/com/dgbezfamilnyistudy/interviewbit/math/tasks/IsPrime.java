package com.dgbezfamilnyistudy.interviewbit.math.tasks;

/*

Verify Prime

Given a number N, verify if N is prime or not.

Return 1 if N is prime, else return 0.

Example :

Input : 7
Output : True

*/
public class IsPrime {
	public static boolean isPrime(int number) {
		if ( number == 1 ) {
			return false;
		}
		for ( int i = 2; i <= Math.sqrt( number ); i++ ) {
			if ( number % i == 0 ) {
				return false;
			}
		}

		return true;
	}
}
